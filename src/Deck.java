//Keep this for now
import java.util.Arrays;


public class Deck{
	
	private Card[] deck;
	private int dealLoc;
	
	public Deck(){
		dealLoc = 0;
		deck = new Card[52];
		//int cardsMade = 0;
		for (int suits = 0; suits < 4; suits++){
			for (int cardVal = 1; cardVal < 14; cardVal++){
				deck[(suits * 13) + cardVal - 1] = new Card(cardVal, suits);
				//cardsMade++;
			}
		}
		shuffle();
	}
	
	public void shuffle(){
		for (int i = 0; i < deck.length; i++) {
			// move element i to a random index in [i .. length- i]
			int randomIndex = (int) (Math.random() * deck.length - i);
			swap(deck, i, i + randomIndex);
		}
	}
	
	private void swap(Card[] a, int i, int change) {
		if (i != change) {
			Card temp = a[i];
			a[i] = a[change];
			a[change] = temp;
		}
	  }
	
	//do something here
	public void deal(){
		dealLoc++;
		//something else
		
	}
	
	public void sortCards(){
		//lets do some quickSort or bogoSort
	}
	
	public boolean isSorted(){
		for (int i = 1; i < deck.length; i++){
			//i think thats how it is?
			if (deck[i].compareTo(deck[i - 1]) > 1){
				
				return false;
			}
		}
		return true;
	}
	
	public boolean isEmpty(){
		return dealLoc < deck.length;
	}
	
	public String toString(){
		//TODO: change this to be a better representation
		return Arrays.toString(deck);
	}
	
	public boolean containsCard(Card lookingFor){
		if (lookingFor == null){
			throw new IllegalArgumentException("The card passed in is null");
		}
		//TODO: make this find if the card is not dealt
		return false;
	}
	
	
}
