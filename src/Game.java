
/** 
 * This is an interface for 
 * card games, they all have a deck,
 * rules, and the acutal game
 */
public interface Game {
	public Deck deck();
	public Hand hand();
	public Rules gameRules();
	public cardGame theGame();
	
}
