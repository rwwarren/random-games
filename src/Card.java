/**
 * Creates the cards that
 * are used in that card game
 */
public class Card implements Comparable<Card>{
	
	private int value;
	private int suit;
	
	/**
	 * Creates each card to 
	 * be used in the game.
	 * Uses ints as values and
	 * suits, values are 0-13
	 * and suits are 0-3
	 * otherwise it will throw an
	 * IllegalArgumentException
	 */
	public Card(int value, int suit){
		if (value < 1 || value > 13){
			throw new IllegalArgumentException("Illegal value of Card");
		} else if (suit < 0 || suit > 3){
			throw new IllegalArgumentException("The suit is illegal");
		}
		this.value = value;
		this.suit = suit;
	}
	
	/**
	 * Returns the string of the 
	 * card that is value suit 
	 */
	public String toString(){
		String result = "";
//		if (value == 1 || value > 10){
//			if (value == 1){
//				result += "Ace ";
//			} else if (value == 11) {
//				result += "Jack ";
//			} else if (value == 12){
//				result += "Queen ";
//			} else { //value == 13
//				result += "King ";
//			}
//		} else {
//			result += value + " ";
//		} 
		result += getValue() + " ";
		//something about the suit here
//		if (suit == 0){
//			result += "Hearts";
//		} else if (suit == 1){
//			result += "Diamonds";
//		} else if (suit == 2){
//			result += "Clubs";
//		} else { //suit == 3
//			result += "Spades";
//		}
		result += getSuit();
		return result;
	}
	
	public String getSuit(){
		if (suit == 0){
			return "Hearts";
		} else if (suit == 1){
			return "Diamonds";
		} else if (suit == 2){
			return "Clubs";
		} else { //suit == 3
			return "Spades";
		}
	}
	
	public String getValue(){
		if (value == 1 || value > 10){
			if (value == 1){
				return "Ace";
			} else if (value == 11) {
				return "Jack";
			} else if (value == 12){
				return "Queen";
			} else { //value == 13
				return "King";
			}
		} else {
			return "" + value;
		}
	}
	
	//TODO:
	//some way to compare the cards
	public int compareTo(Card other){
		return -1;
	}
	
}
